'use strict'

const App = require('express')()


App.get('/', (req, res) => res.json({ message: 'ok' }))
App.get('/crash', (req, res) => {

    setTimeout(() => {
        console.log(req.name.title)
    }, 500)
})

App.listen(process.env.PORT || 80, () => {
    console.log('App started')
    console.log('env')
    console.log(process.env)
})